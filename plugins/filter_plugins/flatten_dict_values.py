# This function will take a dictionary composed of sub arrays
# and flatten it
# e.g.
# [[u'46.101.45.8', u'10.131.174.53'], [u'46.101.45.10', u'10.131.174.54']]
#  to
#  [u'46.101.45.8', u'10.131.174.53', u'46.101.45.10', u'10.131.174.54']

def flatten_dict_values(dictionary):
    result = []
    #result = reduce(operator.add, dictionary.itervalues())
    #result = reduce(list.__add__, dictionary)
    result = reduce(list.__add__, dictionary,[])
    return result


class FilterModule (object):
    def filters(self):
        return {
            "flatten_dict_values": flatten_dict_values
        }

# http://feldboris.alwaysdata.net/blog/python-trick-how-to-flatten-dictionaries-values-composed-of-iterables.html
# http://stackoverflow.com/questions/15995/useful-code-which-uses-reduce-in-python
