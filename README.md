# README #

This is a demo Ansible folder.

## Usage

Download this (as opposed to cloning it) to form the base of a new project.
Edit as required and update the readme specific to your project.
A sample base role is provided that you can use.

and that will create a role folder structure for you to use.




## Running the playbook

Place details about running your playbook here 


ansible-playbook playbook.yml -i hosts -u username -k -v

## Notes:
Using the {{ inventory_hostname variable }} vs the {{ ansible_hostname }}
````
 ________________________________________
/ inventory_hostname variable - This is  \
| the host name we use to connect to the |
\ server.                                /
 ----------------------------------------
        \   ^__^
         \  (oo)\_______
            (__)\       )\/\
                ||----w |
                ||     ||
                
 ____________________________________
/ ansible_hostname - refers to the   \
\ hostname set on the server itself. /
 ------------------------------------
  \
   \
       __
      UooU\.'@@@@@@`.
      \__/(@@@@@@@@@@)
           (@@@@@@@@)
           `YY~~~~YY'
            ||    ||
````

